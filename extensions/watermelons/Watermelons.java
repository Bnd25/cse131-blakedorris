package watermelons;

import java.util.Arrays;

public class Watermelons {
	
	/**
	 * Computes the sum of each distinct pair of entries in the incoming array.
	 * A given pair of entries has its sum computed only once.  So if you
	 * compute the sum for entries 2 and 4, and that sum appears in your answer
	 * array, then you do not also compute the sum of entries 4 and 2.
	 * Depending on the input, the same sum value may appear multiple times in the result.
	 * For example, if all of the incoming values are 0, then so are all of the returned values.
	 * @param nums an array of integers, possibly containing duplicates
	 * @return an array containing the sums of pairs as described above
	 */
	public static int[] allPairSums(int[] numbers) {
		int k=0;
		for (int l=0; l<numbers.length; l=l+1){
			k=k+l;
		}
		int[] answer = new int[k] ;
		int number=0;
		for (int l=0; l<numbers.length; l=l+1){
			for (int p=0; p<numbers.length; p=p+1){
				if (l<p){
					answer[number]=numbers[l]+numbers[p];
					number=number+1;// FIXME compute ans as the result you want
				}
			}
		}
		return answer;
	}
	
	/**
	 * The method below must COMPUTE and return a solution as described
	 * on the web page for this extension.  
	 * 
	 * You must compute the solution by trying
	 * lots of possibilities, and finding the one that yields the right answer.
	 * 
	 * You can run the provided unit test to see if you are right.
	 * @param pairSums is array of watermelon pairwise sums.  In other words,
	 *    each element of pairSums represents the sum of one pair of watermelons in our puzzle.
	 * @return
	 */
	public static int[] getSolution(int[] pairSums) {
		int [] solution= new int [5];
		int [] solution2= new int [5];
		for (int l=0; l<25; l=l+1){
			for (int p=0; p<25; p=p+1){
				for (int m=0; m<25; m=m+1){
					for (int n=0; n<25; n=n+1){
						for (int j=0; j<25; j=j+1){
							solution[0]=l;
							solution[1]=p;
							solution[2]=m;
							solution[3]=n;
							solution[4]=j;
							solution2= allPairSums(solution);
							if (sameIntArrays(solution2,pairSums)){
								return solution;
							}
						}
					}
				}
			}
		}
		return new int[] { 0 };
	}
	
	/**
	 * Compare two arrays for equality.  They must first be
	 * sorted, so that Arrays.equals can do its thing element
	 * by element, as is it wants.
	 * 
	 * However, what if the caller doesn't want the arrays to
	 * be disturbed?  We therefore clone the arrays (copies are
	 * made of them) before we do the compare, and we compare the
	 * clones.
	 * @param one: an array, not mutated
	 * @param two: another array, not mutated
	 * @return true iff the arrays' contents are the same
	 */
	public static boolean sameIntArrays(int[] one, int[] two) {
		int[] copy = (int[]) one.clone();
		int[] copy2 = (int[]) two.clone();
		Arrays.sort(copy);
		Arrays.sort(copy2);
		return Arrays.equals(copy, copy2);
	}


}
package project;

public class PowerUp {

	private Player p;
	private Player p2;
	private double positionX;
	private double positionY;
	private boolean secondChance;
	private double percentChance;
	private boolean bigBallStatus;
	private boolean gravityStatus;
	private boolean useGravity;
	private boolean gravityStatusP2;
	private boolean useGravityP2;
	public PowerUp(){
		this.p = p;
		this.p2 = p2;

		this.positionX =0;
		this.positionY = .025;
		this.secondChance = false;
		this.percentChance = percentChance;
		this.bigBallStatus= false;
		this.gravityStatus = false;
		this.useGravity = false;
		this.gravityStatusP2 = false;
		this.useGravityP2 = false;
	}
	public void bigBall(Player p, double velocity, double angle){
		p.ThrowObject(velocity, angle, .02);
		
		//Object name.bigBall(velocity, angle);
	}
	public boolean bigBallActivate(){
		bigBallStatus = true;
		return bigBallStatus;
	}
	/**
	 * 
	 * @return a deactivated bigBall
	 */
	public boolean bigBallDeactivate(){
		bigBallStatus = false;
		return bigBallStatus;
	}
	/**
	 * 
	 * @return bigBall status
	 */
	public boolean getbigBall(){
		return bigBallStatus;
	}
	/**
	 * 
	 * @return a true gravity status; sets it back to true
	 */
	public boolean ignoreGravityActivate(){
		gravityStatus = true;
				return gravityStatus;
	}
	/**
	 * 
	 * @return gravity status back to false
	 */
	public boolean ignoreGravityDeactivate(){
		gravityStatus = false;
				return gravityStatus;
	}
	/**
	 * 
	 * @return the value of gravity status
	 */
	public boolean getGravity(){
		return gravityStatus;
	}
	/**
	 * changes useGravity to true
	 */
	public void activateUseGravity(){
		this.useGravity = true;
	}
	/**
	 * changes useGravity to false
	 */
	public void deactivateUseGravity(){
		this.useGravity = false;
	}
	/**
	 * 
	 * @return changes the gravityStatusP2 to true
	 */
	public boolean ignoreGravityActivateP2(){
		gravityStatusP2 = true;
				return gravityStatusP2;
	}
	/**
	 * 
	 * @return changes gravityStatusP2 to false
	 */
	public boolean ignoreGravityDeactivateP2(){
		gravityStatusP2 = false;
				return gravityStatusP2;
	}
	/**
	 * 
	 * @return returns the gravityStatusP2 value at the current moment
	 */
	public boolean getGravityP2(){
		return gravityStatusP2;
	}
	/**
	 * changes useGravityP2 to true
	 */
	public void activateUseGravityP2(){
		this.useGravityP2 = true;
	}
	/**
	 * changes useGravityP2 to false
	 */
	public void deactivateUseGravityP2(){
		this.useGravityP2 = false;
	}
	/**
	 * 
	 * @return the current useGravity value
	 */
	public boolean getUseGravity(){
		return useGravity;
	}
	/**
	 * 
	 * @return the value of useGravityP2 at the current moment
	 */
	public boolean getUseGravityP2(){
		return useGravityP2;
	}
	/**
	 * 
	 * @return sets secondChance to true
	 */
	public boolean secondChanceactivate(){
		secondChance = true;

		return secondChance;

	}
	/**
	 * 
	 * @return changes secondChance to false
	 */
	public boolean secondChanceDeactivate(){
		secondChance = false;

		return secondChance;

	}
	/**
	 * 
	 * @return the status of the second chance power up
	 */
	public boolean getSecondChance(){
		return secondChance;
	}
	/**
	 * 
	 * @return x coord of the center of the power up
	 */
	public double getXCoordinates(){
		return this.positionX;
	}
	/**
	 * 
	 * @return y coord of the center of the power up
	 */
	public double getYCoordinates(){
		return this.positionY;
	}
/**
 * assign an x value towards the center of the board for the power up
 */
	public void setXCoordinates(){
		this.positionX = Math.random();
		while (positionX<.4||positionX>.65){
			this.positionX = Math.random();
		}
	}
	/**
	 * 
	 * @return sets the chance that power up will activate
	 */
	public double setPercentChance(){
		percentChance = Math.random();
		return this.percentChance;
	}
	/**
	 * 
	 * @return percentChance
	 */
	public double getPercentChance(){
		
		return percentChance;
	}
	/**
	 * assign an y value towards the center of the board for the power up
	 */
	public void setYCoordinates(){
		this.positionY = Math.random();
		while (positionY<.4||positionY>.6){
		this.positionY = Math.random();
		}
	}
}

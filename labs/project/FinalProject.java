package project;

import java.awt.Color;


import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class FinalProject {

	public static void main(String[] args) {

		while(true){
			ArgsProcessor ap = new ArgsProcessor(args);


			int games = ap.nextInt("What do you want to play to?");
			String player1Name = ap.nextString("What is player 1 called?");
			String player2Name = ap.nextString("What is player 2 called?");

			PowerUp powerUp = new PowerUp();
			Player player1 = new Player(player1Name, powerUp);
			Player player2 = new Player(player2Name, powerUp);

			Board game = new Board(player1, player2, powerUp);






			while(player1.playerWins()<games&&player2.playerWins()<games){
				player1.PlayerAlive();
				player2.PlayerAlive();
				StdDraw.setPenColor();
				player1.setXCoordinatesLeft();
				//	player1.setYCoordinates();
				player2.setXCoordinatesRight();


				//	player2.setYCoordinates();

				//	StdDraw.line(.5, 1, .5, 0);
				game.draw();
				StdDraw.show();
				boolean player2Start;

				if(Math.random()>.5){
					player2Start = false;
				}
				else {
					player2Start = true;
				}


				while(player1.getStatus()==true && player2.getStatus()==true){


					//				player1.DrawPlayer();
					//				player2.DrawPlayer();
					if(player2Start==false){
						if(powerUp.getGravity() == true){
							powerUp.activateUseGravity();
						}
						powerUp.setXCoordinates();
						powerUp.setYCoordinates();
						powerUp.setPercentChance();
						game.powerUpNotHit();
						StdDraw.clear(Color.cyan);
						game.draw();
						StdDraw.show();
						double velocity = ap.nextInt(player1Name + ", input a velocity.");
						while(velocity<1||velocity>50){
							velocity = ap.nextInt("Please input a valid number.");
						}
						int angle = ap.nextInt(player1Name + ", input an angle.");
						while(angle>180||angle<-180){
							angle = ap.nextInt("Please input valid angle.");
						}

						player1.setballLocationToInitial();
						player1.setTime();
						while(player1.getBallLocationX()>0&&player1.getBallLocationX()<1&&player1.getBallLocationY()>0.012&&player2.getStatus()==true){

							if(Math.sqrt((Math.pow((player1.getBallLocationX()-powerUp.getXCoordinates()), 2)+ (Math.pow(player1.getBallLocationY()-powerUp.getYCoordinates(), 2)))) <=.055&&game.getPowerUpHit()==false)
							{
								int d = (int)(Math.random()*3.0);
								if(d==0){
									powerUp.bigBallActivate();
								}
								if(d==1){
									powerUp.secondChanceactivate();
									System.out.println("test");
								}
								if(d==2){
									powerUp.ignoreGravityActivate();
								}
								game.powerUpHit();
							}
							//						if(player has trishot powerup){
							//							powerup object.triShot(velocity, angle);
							//						} else {...
							player1.ThrowObject(velocity/4000.0, angle*Math.PI/180, player1.getRadius());
							StdDraw.show(2);

							StdDraw.clear(Color.cyan);

							game.draw();
							if(powerUp.getbigBall()==true){
								if(Math.sqrt((Math.pow((player1.getBallLocationX()-player2.getXCoordinate()), 2)+ (Math.pow(player1.getBallLocationY()-player2.getYCoordinate(), 2)))) <=.045
										){
									
									player2.PlayerHit();
									StdDraw.clear(Color.cyan);

									game.winner(player1);
									StdDraw.show(3000);
									player1.addWin();
									StdDraw.clear(Color.cyan);
								}
							}
							else{
								if(powerUp.getGravityP2() == true){
									powerUp.activateUseGravityP2();
								}
								if(Math.sqrt((Math.pow((player1.getBallLocationX()-player2.getXCoordinate()), 2)+ (Math.pow(player1.getBallLocationY()-player2.getYCoordinate(), 2)))) <=.016){
							
								player2.PlayerHit();
								StdDraw.clear(Color.cyan);

								game.winner(player1);
								StdDraw.show(3000);
								player1.addWin();
								StdDraw.clear(Color.cyan);
							}
							}
						}
						if(powerUp.getbigBall()==true){
							powerUp.bigBallDeactivate();
							player2Start = true;
						}
						if(powerUp.getSecondChance() ==true){
							powerUp.secondChanceDeactivate();
						}

						else{
							player2Start=true;
						}
						if(powerUp.getUseGravity()==true){
							powerUp.deactivateUseGravity();
							powerUp.ignoreGravityDeactivate();
							player2Start = true;
						}
					}
					else{
						if(powerUp.getGravityP2() == true){
							powerUp.activateUseGravityP2();
						}
						powerUp.setXCoordinates();
						powerUp.setYCoordinates();
						powerUp.setPercentChance();
						game.powerUpNotHit();
						StdDraw.clear(Color.cyan);
						game.draw();
						StdDraw.show();
						double velocity = ap.nextInt(player2Name + ", input a velocity from 1 to 50.");
						while(velocity<1||velocity>50){
							velocity = ap.nextDouble("Please input a valid number.");
						}
						double angle = ap.nextInt(player2Name + ", input an angle from -180 to 180 degrees.");
						while(angle>180||angle<-180){
							angle = ap.nextInt("Please input valid angle.");
						}
						player2.setballLocationToInitial();
						player2.setTime();
						while(player2.getBallLocationX()>0&&player2.getBallLocationX()<1&&player2.getBallLocationY()>0.012&&player1.getStatus()==true){

							if(Math.sqrt((Math.pow((player2.getBallLocationX()-powerUp.getXCoordinates()), 2)+ (Math.pow(player2.getBallLocationY()-powerUp.getYCoordinates(), 2)))) <=.055&&game.getPowerUpHit()==false)
							{
								int d = (int)(Math.random()*2.0);
								if(d==0){
									powerUp.bigBallActivate();
								}
								if(d==1){
									powerUp.secondChanceactivate();
									System.out.println("test");
								}
								if(d==2){
									powerUp.ignoreGravityActivateP2();
								}
								game.powerUpHit();
								
							}
							


							if(powerUp.getbigBall()==true){
								powerUp.bigBall(player2, (velocity/4000.0), (180-angle)*(Math.PI)/180);
							}
							else{
								player2.ThrowObject(velocity/4000.0, (180-angle)*(Math.PI)/180, player2.getRadius());
							}
							StdDraw.show(2);
							StdDraw.clear(Color.cyan);
							game.draw();
							if(powerUp.getbigBall()==true){
								if(Math.sqrt((Math.pow((player2.getBallLocationX()-player1.getXCoordinate()), 2)+ (Math.pow(player2.getBallLocationY()-player1.getYCoordinate(), 2)))) <=.046&&game.getPowerUpHit()==false){
									player1.PlayerHit();
									StdDraw.clear(Color.cyan);

									game.winner(player2);

									StdDraw.show(3000);
									StdDraw.clear(Color.CYAN);

									player2.addWin();
								}
							}
							else{
								if(Math.sqrt((Math.pow((player2.getBallLocationX()-player1.getXCoordinate()), 2)+ (Math.pow(player2.getBallLocationY()-player1.getYCoordinate(), 2)))) <=.016){
								player1.PlayerHit();
								StdDraw.clear(Color.cyan);
								
								game.winner(player2);

								StdDraw.show(3000);
								StdDraw.clear(Color.CYAN);

								player2.addWin();
							}
							}
						}
						if(powerUp.getbigBall()==true){
							powerUp.bigBallDeactivate();
							player2Start = false;
						}
						if(powerUp.getSecondChance() ==true){
							powerUp.secondChanceDeactivate();
						}
						else{
							player2Start = false;
						}
						if(powerUp.getUseGravity()==true){
							powerUp.deactivateUseGravityP2();
							powerUp.ignoreGravityDeactivateP2();
							player2Start = true;
						}
						
					}






				}
			}
			StdDraw.setPenColor();
			StdDraw.text(.5, .5, "Game Over, Play Again?");
			StdDraw.show(4000);


		}
	}
}

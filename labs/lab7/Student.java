package lab7;

public class Student {

	private String firstName = this.firstName;
	private String lastName = this.lastName;

	private int id, credits;
	
	private double gpa; 
	
	
	
	
	public Student(String firstName, String lastName, int id){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.credits = 0;
		this.gpa = 0.0;
	}
	public String getName(){
		return ""+firstName + " "+ lastName;	}
	public double getGPA(){
		return gpa;
	}
	public int getCredits(){
		return credits;
	}
	public int getStudentID(){
		return id;
	}

	public String getClassStanding(){
		if(credits<30 ){
			return "Freshman";
		}
		if(credits>=30&&credits<60){
			return "Sophomore";
		}
		if(credits>=60&&credits<90){
			return "Junior";
		}if(credits>=90){
			return "Senior";
		}
		else {return "";}
	}

	public double submitGrade(double courseGrade, int creditsForClass) {
		double qualityScore = (creditsForClass*courseGrade)+(this.gpa*this.credits);
		this.credits  = this.credits + creditsForClass;
		this.gpa = Math.round(((double)qualityScore/(double)this.credits)*1000.0)/1000.0;
		
		
		return this.gpa;
	}
	

	public Student (String firstName, String lastName, int id, double estimatedGPA, int expectedCredits){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.credits = expectedCredits;
		this.gpa = estimatedGPA;
	}


		public Student createLegacy(Student otherStud){
			String babyName = getName();
			String babyLastName = otherStud.getName();
			int babyID = getStudentID() + otherStud.getStudentID();
			double babyGPA = (getGPA()+otherStud.getGPA())/2.0;
			int babyCredits = Math.max(getCredits(), otherStud.getCredits());
			
			return new Student(babyName, babyLastName, babyID, babyGPA, babyCredits);
		}
		public String toString(){
			return getName() + getStudentID();
		}
		
		


}


package exam2;

public class ExamTemplate {
	public static void main(String[] args) {
		//Yes, this is supposed to be blank

		//Use this method to test the methods that we ask you
		//to write
		System.out.println(dragon(2));
		System.out.println(dragon(1));
		System.out.println(dragon(0));
		System.out.println(koch(2));
		System.out.println(koch(1));
		System.out.println(koch(0));

	}
	public static String koch(int x) {
		String result = "";
		String kochString = "F+F-F-F+F";

		
		if(x==0){
			return "F";
		}
		if(x>=1){
			for(int i = 1; i<x;++i){
				kochString =  kochString.replaceAll("F", "F+F-F-F+F");
				kochString = kochString;
			}
			return kochString;
		}
		else return "";
		
		//I have no idea how to do this recursively. I know you're supposed to use the method in the answer but I couldn't tell you how
		//I was able to do this one iterively though, since it only had one variable
	}
	public static String dragon(int x){
		String result = "";
		String dragonString = "F-H";
		for(int i = 0; i<=x; ++i){

			if(x>0){
				dragonString =  dragonString.replaceAll("F", "F")  +"-" +dragonString.replaceAll("-H", "+H");
				//I thought I could just concatenate these, but it always carries over the
				//letter not replaced by the operation
				//
			}
			else {
				return dragonString;
			}
		}

		return dragonString;

	}
	public static double computeScale(int x) {
		return 66.6;
	}
	//The methods that we ask you to write should go here
	//well shit, that was fun
}

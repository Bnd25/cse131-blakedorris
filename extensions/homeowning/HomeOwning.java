package homeowning;

import cse131.ArgsProcessor;

public class HomeOwning {

	public static void main(String[] args) {
	
		ArgsProcessor ap = new ArgsProcessor(args);

		String apartmentName = ap.nextString("What is the name of the apartment complex?");
		String apartmentZip = ap.nextString("What is the apartments zip code?");
		double rentCost = ap.nextDouble("What is the monthly rent for the apartment complex?");
		double dailyInterest = ap.nextDouble("What is the daily interest on the mortgage?");
	
		double yearlyRent = Math.round((rentCost *12.0)*(double)100.0)/100.0;
		double weeklyRent = Math.round((yearlyRent/52.0)*(double)100.0)/100.0;
		
		double weeklyInterest = Math.round((dailyInterest * 7.0)*(double)100)/100.0;
		double yearlyInterest = Math.round((dailyInterest *365.0)*(double)100)/100.0;
		System.out.println(apartmentName + " is located in the Zip Code " + apartmentZip+"." + "\n");
		System.out.println("Rent per year: "+ yearlyRent );
		System.out.println("Rent per week: "+ weeklyRent+ "\n");
		System.out.println("Interest paid per year: " + yearlyInterest);
		System.out.println("Interest paid per week: " + weeklyInterest);
	
		if (weeklyInterest <weeklyRent){
			System.out.println("I should buy.");
		}
		if (weeklyInterest >weeklyRent){
			System.out.println("I should rent.");
		}
	//If the rent was $980 per month and the daily interest is 31.99, it is
	//a better option to buy at that point.
	
	
	}

}

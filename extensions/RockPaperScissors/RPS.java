package RockPaperScissors;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int games = ap.nextInt("How many games would you like to play?");
		int totalWins = 0;
		int opponentWins = 0;
		for (int i = 0; i<games; ++i) {
				
		int moveChoice = ap.nextInt("Would you like to play rock, paper, or scissors? \n Input 1 for rock, 2 for paper, or 3 for scissors. ");
		if (moveChoice == 1) {
			System.out.println("You have chosen to play rock.\n");	
		}
		if (moveChoice == 2) {
			System.out.println("You have chosen to play paper.\n");	
		}
		if (moveChoice == 3) {
			System.out.println("You have chosen to play scissors. \n");	
		}
		int opponentsMove = (int)(Math.random()*3)+1;
		
		if (opponentsMove == 1){
			System.out.println("Your opponent plays rock.");
		}
		if (opponentsMove == 2){
			System.out.println("Your opponents plays paper.");
		}
		if (opponentsMove==3){
			System.out.println("Your opponent plays scissors.");
		}
		if (moveChoice == 1  && opponentsMove == 3 ){
			System.out.println("Its rock against scissors. You win!");
			totalWins = totalWins + 1;
			
		}
		if (moveChoice == 2 && opponentsMove == 3){
			System.out.println("It's paper against scissors. You lose...");
			opponentWins = opponentWins +1;
		}
		if (moveChoice == 3 && opponentsMove == 3){
			System.out.println("It's paper against paper. It's a tie.");
		}
		if (moveChoice == 1 && opponentsMove == 2){
			System.out.println("It's rock against paper. You lose...");
			opponentWins = opponentWins +1;
		}
		if (moveChoice == 1 && opponentsMove == 1){
			System.out.println("It's rock against rock. It's a tie.");
		}
		if (moveChoice == 2 && opponentsMove == 2){
			System.out.println("It's paper against paper. It's a tie.");
		}
		if (moveChoice == 2 && opponentsMove == 1){
			System.out.println("Its paper against rock. You win!");
			totalWins = totalWins + 1;
		}
		if (moveChoice == 3 && opponentsMove == 2){
			System.out.println("It's scissors against paper. You win!");
			totalWins = totalWins + 1;
		}
		if (moveChoice == 3 && opponentsMove == 1){
			System.out.println("It's scissors against rock. You lose...");
			opponentWins = opponentWins +1;
			
		}
		System.out.println("");
		
		}
		System.out.println("You won " + (double)totalWins/games*100 + "% of the games.");
		System.out.println("Your opponent won " +(double)opponentWins/games*100 + "% of the games.");
		System.out.println("Ties made up the remaining percent.");
		}

	}



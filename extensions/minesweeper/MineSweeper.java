package minesweeper;

import java.util.Objects;

import cse131.ArgsProcessor;

public class MineSweeper {

	public static void main (String[] args) {
		
		//
		// Do not change anything between here ...
		//
		ArgsProcessor ap = new ArgsProcessor(args);
		int cols = ap.nextInt("How many columns?");
		int rows = ap.nextInt("How many rows?");
		double percent = ap.nextDouble("What is the probability of a bomb?");
		//
		// ... and here
		//
		//  Your code goes below these comments
		//
		String [][] bomb= new String [rows+2][cols+2];
		int mines[][]= new int [rows+2][cols+2];
		double b=0.0;
		int num=0;
		for (int k=1; k<rows+1; k=k+1){
			for (int l=1; l<cols+1; l=l+1){
				b = Math.random();
				if (b<percent){
					bomb[k][l]="*";
				}
				else {
					bomb[k][l]=".";
				}
			}
		}
		/*
		 * 
		 */
		for (int k=1; k<rows+1; k=k+1){
			for (int l=1; l<cols+1; l=l+1){
				if (Objects.equals("*", bomb[k][l])){
				mines[k][l]=-1;
				}
				else{
					for (int r=k-1; r<=k+1; r=r+1){
						for (int c=l-1; c<=l+1; c=c+1){
							if (Objects.equals("*", bomb[r][c])){
								mines[r][c]=num+1;
								num=num+1;
							}
						}
					}
					num=0;
				}
			}
			
				}
		/*
		 * 
		 */
		for (int k=1; k<rows+1; k=k+1){
			for (int l=1; l<cols+1; l=l+1){
				System.out.print(bomb[k][l]+" ");
			}
					System.out.print("  ");	
					for (int l=1; l<cols+1; l=l+1){
						if (mines[k][l]==-1){
							System.out.print("*"+" ");
						}
						else {
							System.out.print(mines[k][l]+" ");
					}
				}
					System.out.println();
			}
		}
}
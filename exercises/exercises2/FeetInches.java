package exercises2;

import cse131.ArgsProcessor;

public class FeetInches {

	public static void main(String[] args) {
		//
		// Prompt the user for a number of inches
		//
		// Convert that into feet and inches
		//   BUT
		// Be sure to use the singular "foot" or "inch"
		//   where appropriate, as discussed in
		//   the introductory video
		//
		// For example, 61 inches would produce
		//    the output
		//   5 feet and 1 inch
		//
			ArgsProcessor ap = new ArgsProcessor(args);
			
			double inches = ap.nextDouble("Input your value in inches.");
				double feet = ((int)inches) /12;
				double inchesleft = inches % 12.0;
				
				
			boolean foot = feet == 1;
			boolean inch = inchesleft == 1;
			
			
			if (foot){
				if(inch){System.out.println(inches + " inches is equivalent to " + feet + " foot and " + inchesleft + "inch.");}
				else {System.out.println(inches + " inches is equivalent to " + feet + " foot and " + inchesleft + "inches.");}}
			else {System.out.println(inches + " inches is equivalent to " + feet + " feet and " + inchesleft + "inches.");}
			
		if (!foot) {
			if (inch) {System.out.println(inches + " inches is equivalent to " + feet + " foot and " + inchesleft + "inch.");}
			}
			}
	

}

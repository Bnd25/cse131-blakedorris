package fibonacci;

public class Fibonacci {
	
	/**
	 * Below copy your solution to recursive Fibonacci from studio
	 * @param n
	 * @return fib(n), computed recursively
	 */
	public static int fib(int n) {
		if (n>1){
			int fibTotal=	fib(n-1) + fib(n-2)	;
			return fibTotal;
		}
		else {
			return n;
		}
	}
		/**
	 * Below write your solution to Fibonacci, using iteration
	 * @param n
	 * @return fib(n), computed iteratively
	 */
	public static int iterative(int n) {
		int x = 0;
		int [] fib = new int [n+2];
		for(int i =0; i<=2;++i){
		x = i;
		
		fib[0] = 0;
		fib[1] = 1;
		fib[2] = 1;
		}
		for (int i =3; i<=n; ++i){
			fib[i] = fib[i-1]+fib[i-2];
		
			x= i;
		}
		return fib[x];
		
		
	}

}

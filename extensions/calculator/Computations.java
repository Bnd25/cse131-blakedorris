package calculator;

public class Computations {

	/**
	 * Create the static methods as described on the 
	 * description for this assignment.
	 */
	/**
	 * 
	 * @param d1 double input 1
	 * @param d2 double input 2
	 * @return sum of inputs
	 */
	public static double addDoubles(double d1, double d2){
		return d1+d2;
	}
	/**
	 * 
	 * @param d1 double input
	 * @param d2 double input
	 * @return diffference of doubles
	 */
	public static double subtractDoubles(double d1, double d2){
		return d1-d2;
	}
	/**
	 * 
	 * @param d1 double input
	 * @param d2 double input
	 * @return quotient of inputs
	 */
	public static double divideDoubles (double d1, double d2){
		if(d2!=0){return d1/d2;}
		else return 0;
	}
	/**
	 * 
	 * @param d1
	 * @param d2
	 * @return product of inputs
	 */
	public static double multiplyDoubles (double d1, double d2){
		return d1*d2;
	}
	/**
	 * 
	 * @param in String input
	 * @return error
	 */
	public static int stringToInt(String in) {
		throw new UnsupportedOperationException();
	}
	/**
	 * 
	 * @param i1 integer value
	 * @param i2 integer value
	 * @return sum of inputs 
	 */
	public static int addInts(int i1, int i2){
		return i1 + i2;
	}
	/**
	 * 
	 * @param i1
	 * @param i2 number that is being sutracted
	 * @return i1-i2
	 */
	public static int subtractInts(int i1, int i2){
		return i1-i2;
	}
	/**
	 * 
	 * @param i1
	 * @param i2
	 * @return i1*i2
	 */
	public static int multiplyInts(int i1, int i2){
		return i1*i2;
	}
	/**
	 * 
	 * @param i1 integer 1
	 * @param i2 integer 2
	 * @return i1/i2
	 */
	public static int divideInts(int i1, int i2) {
		if(i2!=0){
			return i1/i2;
		}
		else return 0;
	}
	/**
	 * 
	 * @param s1 String
	 * @param s2 String input 2
	 * @return "String"+"String"
	 */
	public static String concatenate(String s1, String s2){
		return s1+s2;
	}
	/**
	 * 
	 * 
	 * @param b1 true/false
	 * @param b2 true/false
	 * @returntrue/false
	 */
	public static boolean andBoolean(boolean b1, boolean b2){
		return b1 && b2;
	}
	/**
	 * 
	 * @param b1 true/false
	 * @param b2true/false
	 * @return true
	 */
	public static boolean orBoolean(boolean b1, boolean b2){
		return b1 || b2;
	}
	/**
	 * 
	 * @param in integer input
	 * @return the input as a double
	 */
	public static double intToDouble(int in){
		return (double)in;
	}
	/**
	 * 
	 * @param in double input
	 * @return the same double
	 */
	public static double doubleToDouble(double in){
		return in;
	}
	/**
	 * 
	 * @param in String input
	 * @return error
	 */
	public static double stringToDouble(String in){
		throw new UnsupportedOperationException();
	}
	
	/**
	 * 
	 * @param in
	 * @return error
	 */
	public static double booleanToDouble(boolean in){
		throw new UnsupportedOperationException();
	}
	/**
	 * 
	 * @param in integer input
	 * @return the input
	 */
	public static int intToInt(int in){
		return in;
	}
	/**
	 * 
	 * @param in double input
	 * @return input as integer
	 */
	public static int doubleToInt(double in){
		return (int)in;
	}
	/**
	 * 
	 * @param in true or false
	 * @return error
	 */
	public static int booleanToInt(boolean in){
		throw new UnsupportedOperationException();
	}
	/**
	 * 
	 * @param in integer input
	 * @return String of integer
	 */
	public static String intToString(int in){
		int toBeString = in;
		String fromInt = ""+in;
		return fromInt;
	}
	/**
	 * 
	 * @param in double input
	 * @return A string of the double
	 */
	public static String doubleToString(double in){
		double toBeString = in;
		String fromDouble = "" + toBeString;
		return fromDouble;
	}
	/**
	 * 
	 * @param in String input
	 * @return in
	 */
	public static String stringToString (String in){
		return in;
	}
	/**
	 * 
	 * @param true or false
	 * @return string true or false
	 */
	public static String booleanToString(boolean in){
		
		if(in == true){
			boolean toBeString = in;
			String fromBoolean = "" + toBeString;
			return fromBoolean;
		}
		if(in ==false){
			boolean toBeString = in;
			String fromBoolean = "" + toBeString;
			return fromBoolean;
		}
		return "0";
		
		
	}
	/**
	 * 
	 * @param integer input
	 * @return error
	 */
	public static boolean intToBoolean(int in){
		throw new UnsupportedOperationException();
	}
	/**
	 * 
	 * @param double input
	 * @return error
	 */
	public static boolean doubleToBoolean(double in){
		throw new UnsupportedOperationException();
	}
	/**
	 * 
	 * @param true or false
	 * @return true or false
	 */
	public static boolean booleanToBoolean(boolean in){
		return in;
	}
	/**
	 * 
	 * @param String input
	 * @return error
	 */
	public static boolean stringToBoolean(String in){
		throw new UnsupportedOperationException();
	}
}

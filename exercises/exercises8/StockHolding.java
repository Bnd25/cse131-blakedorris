package exercises8;

import java.util.HashSet;
import java.util.Set;

public class StockHolding {
	
	private final String name;
	private final String owner;
	private int numberOfShares;
	private double pricePerShare;
	
	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public int getNumberOfShares() {
		return numberOfShares;
	}

	public double getPricePerShare() {
		return pricePerShare;
	}

	public void setNumberOfShares(int numberOfShares) {
		this.numberOfShares = numberOfShares;
	}

	public void setPricePerShare(double pricePerShare) {
		this.pricePerShare = pricePerShare;
	}

	public StockHolding(String name, String owner, double pricePerShare){
		this.name = name;
		this.owner = owner;
		this.numberOfShares = 0; 
		this.pricePerShare = pricePerShare;
		
	}

	@Override
	public String toString() {
		return "StockHolding [name=" + name + ", owner=" + owner + ", numberOfShares=" + numberOfShares
				+ ", pricePerShare=" + pricePerShare + "]";
		
	}
	
	public int buyAStock(){
		this.numberOfShares =+ 1;
		return numberOfShares;
			
	}
	
	
	



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockHolding other = (StockHolding) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}

	public static void main(String[] args) {
		Set<StockHolding> set = new HashSet<StockHolding>();
		StockHolding s1 = new StockHolding("IBM", "Blake", 10.7);
		s1.setNumberOfShares(1200);
		set.add(s1);
		s1.setNumberOfShares(1400);
		set.add(s1);

	}

}

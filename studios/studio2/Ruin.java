package studio2;

import cse131.ArgsProcessor;

public class Ruin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			ArgsProcessor ap = new ArgsProcessor(args);
			
			
			double startAmount = ap.nextDouble("How much money do you start with?");
			double winChance = ap.nextDouble("What is the probability you will win between 0 and 1?");
			double winAmount =  ap.nextDouble("What is the amount you will win?");
			int totalPlays = ap.nextInt("How many times will you play?");
			double lossChance = 1-(winChance);
			double ruin = 0;

		
			
					
					if (lossChance != winChance) {
						ruin = (Math.pow(lossChance/winChance, startAmount)) - (Math.pow(lossChance/winChance, winAmount))/(1-(Math.pow(lossChance/winChance, winAmount)));
					}
			
					else { ruin = (1-(startAmount/winAmount));}
					
					
					double currentAmount = startAmount;
					int sim = 0;
					int plays = 0;
					
					while (sim<totalPlays){sim = sim +1;
						
						while (currentAmount!=0.0 && currentAmount<winAmount);{
							plays = plays + 1;
								
						double win = Math.random();
					
						
						
						if (win<winChance) {
							currentAmount= currentAmount +1;
						}
						else {currentAmount = currentAmount -1;}}
						
					
						

					System.out.println("Simulation " + sim + ": " + plays + " rounds");
					
					
					}
					
					
						
						
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
	}

}

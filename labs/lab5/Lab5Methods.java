package lab5;

public class Lab5Methods {

/**
 * 
 * @param x The initial value that the method starts at
 * @return The sum of the method, adding the initial number, subtracting two, adding
 */
	public static int sumDownBy2(int x) {
		int sum= x;
		for (int i=0; i<((int)x/2+1); ++i) {
			if(i==0) {
				sum = x;
			}
			if (i!=0) {
				sum = sum+(x-2*i);
			}

		}		return sum;

	}
/**
 * 
 * @param x The number of iterations; corresponds to the value in the denominator
 * @return The sum of a Harmonic sequence
 */
	public static double harmonicSum(int x) {
		double sum = 1;
		for (int i=0; i<x; ++i) {
			if(i==0) {
				sum = 1;
			}
			if (i!=0) {
				sum = (double)sum+(double)(1/(i+1.0));
			}
		}

		return sum;}
/**
 * 
 * @param k The power to which the denominator is raised
 * @return The sum of the geometric series 1+1/2+1/4+...!/2^k
 */
	public static double geometricSum(int k) {
		double sum = 1;
		if (k!=0 || k ==0){
			
			for (int i=0; i<=k; ++i) {
				if(i==0) {
					sum = 1;
				}
				if (i!=0) {
					sum = (double)sum+(double)(1.0/(Math.pow(2.0, i)));
				}
			}
		}
		if (k < 0){
			sum = 0;
		}
		return sum;
	}
	/**
	 * 
	 * @param x the number being multiplied
	 * @param y the number of times the first number is added to the sum
	 * @return The total value after running y iterations
	 */
	public static int multPos (int x, int y) {
		int sum = 0;
		for (int i=0; i<y; ++i){
			if (i ==0 ){
				sum = x;
			}
			if (i!=0) {
				sum= sum + x;
			}
			
		}
		return sum;
	}
	/**
	 * 
	 * @param x number being multiplied
	 * @param y number of times
	 * @return product
	 */
	public static int mult (int x, int y) {
		int absX = Math.abs(x);
		int absY= Math.abs(y);
		int product = multPos(absX, absY);
	product = -product;
		if (absX*-1==x &&absY *-1 ==y || absX==x &&absY==y){
		 product=Math.abs(product);
		}
		return product;
			
		
	}
	/**
	 * 
	 * @param x Number beings raised to a power
	 * @param y what it's being raised to
	 * @return The final value of x^y
	 */
	public static int expt (int x, int y){
		int sum = x;
		if (y!=0){for (int i = 0; i < y; ++i){
			if (i==0) {
				sum = x;
			}
		if(i!=0){
			sum = sum * x;
		}
		}}
		if(y ==0){
			sum = 1;
		}
		
		return sum;
	}
	
	
	
	
	
	
	
	
	
	
	
	}

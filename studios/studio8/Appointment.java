package studio8;

import Student.java;
import cse131.ArgsProcessor;
public class Appointment {
	
	
	private final Date date;
	private final Time time;
	public Appointment(Date date, Time time){
		date = date;
		time = time;
		
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}
	Date dDay = new Date(6, 6 , 1944, false);
	Date nineEleven = new Date(9,11, 2001, false);
	Date birthday = new Date (9, 23, 1997, false);
	Date otherBirthday = new Date(7, 25, 1997, true);
	Date taBirthday = new Date(10, 27, 1997, false);
Appointment doctor = new Appointment(birthday, one);
Appointment

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

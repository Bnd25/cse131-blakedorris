package studio6;

public class Methods {
	public static void main(String[] args)
	{System.out.println(fib(6));}

	// Your methods go below this line
	/**
	 * 
	 * @param n integer
	 * @return total of factorial of the integer
	 */
	public static int fact(int n){
		if(n<=0){
			return 1;
		}
		else {
			return n*fact(n-1);
		}
	}
	/** 
	 * 
	 * @param n selected integer
	 * @return sum of the fibonacci sequence
	 */
	public static int fib(int n) {
		if (n>1){
			int fibTotal=	fib(n-1) + fib(n-2)	;
			return fibTotal;
		}
		else {
			return n;
		}
	}
	
	/**
	 * 
	 * @param n selected integer
	 * @return whether or not the number is odd
	 */
	public static boolean isOdd(int n) {
		if(n>0) {
			boolean isOddTotal=	!isOdd(n-1)	;
			return isOddTotal;
		}
		else {
			return false;
		}
	}
	/**
	 * 
	 * @param x selected integer
	 * @param y another selected integer
	 * @return the sum of the two integers
	 */
	public static int sum(int x, int y) {
		if(y>0){
			int sumTotal = sum(x+1, y-1);
			return sumTotal;
		}
		else {
			return x;
		}
	}
	/**
	 * 
	 * @param n Largest/ starting integer
	 * @return sum of the sequence
	 */
	public static int sumDownBy2 (int n) {
		if (n>1) {
			int sumDownBy2Total = sumDownBy2(n-2) + n;
			return sumDownBy2Total;
			
		}
		if (n==1){
			return 1;
		}
		if (n==0) {
			return 0;
		}
		return 0;
	}
	/**
	 * 
	 * @param n smallest value/ furthest right
	 * @return value of the harmonic sequence
	 */
	public static double harmonicSum(int n) {
		if (n>1){
			double harmonicSumTotal = harmonicSum(n-1) + 1.0/n;
			return harmonicSumTotal;
		}
		if (n==1) {
			return 1;
		}
		return 0;
	}
	/**
	 * 
	 * @param a number being multiplied
	 * @param b How many times a is added together
	 * @return Value of the multiplication
	 */
	public static int mult(int a, int b) {
		if (b>0) {
			int multTotal = a + mult(a, b-1);
			return multTotal;
			// let's say that a=5 and b=3
			// 5 + 5 + 5
			// a + mult(a,b-1)
			
		}
		if(b==0) {
			return 0;
		}
		return 0;
	}
}


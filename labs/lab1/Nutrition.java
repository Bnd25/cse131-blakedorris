package lab1;

import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		
		String name = ap.nextString("What food did you eat?");
		double carbs = ap.nextDouble("How many grams of carbs are in your food?");
		double fat = ap.nextDouble("How many grams of fat does your food contain?");
		double protein = ap.nextDouble("How many grams of protein does your food contain?");
		int statedCals = ap.nextInt("How many calories does your food contain?");
		
		
		double caloriesfromcarbs = carbs * 4;
		double caloriesfromprotein = protein * 4; 
		double caloriesfromfat = fat * 9;
		
		double Totalcalories = (caloriesfromfat + caloriesfromprotein + caloriesfromcarbs);
		double unavailablecalories = Math.round((Totalcalories - statedCals)*10)/10;
		
		double fiber = unavailablecalories / 4;
		
		double percentcarbs = (Math.round((caloriesfromcarbs / Totalcalories) *1000))/10.0;
		double percentprotein = (Math.round((caloriesfromprotein / Totalcalories) *1000))/10.0;
		double percentfat = (Math.round((caloriesfromfat/ Totalcalories) *1000))/10.0;
		boolean lowcarb = percentcarbs <= 25;
		boolean lowfat = percentfat <= 15;
		
		double headsortails =Math.random();
		boolean heads = headsortails <.5;
		
		System.out.println(name + " has");
		System.out.println(carbs + " grams of carbohydrates = " + caloriesfromcarbs + " calories.");
		System.out.println(fat + " grams of fat = " + caloriesfromfat + " calories.");
		System.out.println(protein + " grams of protein = " + caloriesfromprotein + " calories.");
		System.out.println("");
		System.out.println("This food is said to have " + statedCals + " (available) calories.");
		System.out.println("With " + unavailablecalories + " unavailable calories, this food has " + fiber +" grams of fiber.");
		System.out.println("");
		
		System.out.println("Approximately");
		System.out.println(percentcarbs + "% of your food is carbohydrates.");
		System.out.println(percentfat + "% of your food is fat.");
		System.out.println(percentprotein + "% of your food is protein.");
		System.out.println("");
		
		
		System.out.println("Is this food acceptable for a low-carb diet? " + lowcarb);
		System.out.println("Is this food acceptable for a low-fat diet? " + lowfat);
		System.out.println("Given you choose heads and then flip a coin to decide whether or not to eat this, will you? " + heads);
		
		
		
		
		
		
	
		
		
		
		
		
		
		
		
	}

}

package arrays;
import cse131.ArgsProcessor;

public class Birthday {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		double people = ap.nextInt("How many people will enter the room?");
		int [][] birthday = new int[12][31];
		int [] monthTotal = new int[12];
		int [] dayTotal = new int [31];
		int sameBirthdays = 0;
		for(int i =0; i<people; ++i){
			birthday[(int)(Math.random()*12.0)][(int)(Math.random()*31.0)] ++;
		}
		for(int j = 0; j<12; ++j){

			for(int k = 0 ;k<31;++k){
				for(int l = 0; l < 12; l++){

					dayTotal[k] += birthday[l][k];
				}
				monthTotal[j] = monthTotal[j] + birthday[j][k];
				if(birthday[j][k]>=2){
				sameBirthdays +=birthday[j][k];	
				}
			}




		}
		System.out.println("per month percentage");
		int sumMonth = 0;
		for(int o = 0; o<12;++o){
			System.out.print("Month " + (o+1) + " "+(Math.round((double)monthTotal[o]/people *1000.0))/10.0+ "\t");
			//			double monthPercent = (double)monthTotal[o]/people*100.0;
			//			System.out.print(monthPercent + "\t");
			sumMonth+= Math.round((double)monthTotal[o]/people *1000.0)/10.0;
		}
		
		System.out.println();
		System.out.println("Average of the months: " + sumMonth/12.0);
		int sumDay = 0;
		for(int p = 0; p<31; ++p){
			System.out.print("Day " + (p+1) + " "+(Math.round((double)(dayTotal[p]/12.0)/people *1000.0))/10.0+ "\t");
			sumDay+=Math.round((double)(dayTotal[p]/12.0)/people *1000.0)/10.0;
		}
		System.out.println();
		System.out.println("Average of the days: " + sumDay/31.0);

		System.out.println();
		System.out.println("Percentage of people with same birthday: " + (Math.round((double)(sameBirthdays)/people *1000.0))/10.0);
	}

}

package conway;

/**
 * 
 *
 */
public class Conway {
	
	private int rows;
	private int cols;
	private boolean [][] conway;


	public Conway(int rows, int cols){
		this.rows=rows;
		this.cols=cols;
		this.conway=new boolean [rows][cols];
	}


	public int getRows(){
		return rows;             // FIXME
	}

	public int getColumns(){
		return cols;             // FIXME
	}

	/**
	 * Sets the current status of the cell at (row, col)
	 * @param b if true, the cell is alive; if false, the cell is dead 
	 * @param row
	 * @param col
	 */
	public void setLiveness(boolean b, int row, int col){
		System.out.print(b);
		conway[row][col]=b;
		System.out.println(conway[row][col]);
		// FIXME
	}


	/**
	 *  
	 * @param row
	 * @param col
	 * @return whether or not a cell at a specific row and column is alive. 
	 *    If row or col is out of bounds, you must return false.
	 */
	public boolean isAlive(int row, int col){
		if (row>=rows || col>=cols){
		return false;  
		}
		if (row<0 || col<0){
			return false;// FIXME
		}
		return conway[row][col];
	}


	/**
	 * Make every cell not alive
	 */
	public void clear(){
		for (int k=0; k<rows; k=k+1){
			for (int l=0; l<cols; l=l+1){
				conway[k][l]= false;
			}
		}
		// FIXME
	}


	/**
	 * Consider the 3x3 cell array that has the cell at (row, col) at its center.
	 * Let's call all cells but that center cell the neighbors of that center cell.
	 * This method returns the number of neighbors that are alive.
	 * 
	 *   n  n  n
	 *   n  c  n
	 *   n  n  n
	 *   
	 *  Above, if c represents the cell at (row, col), each n is
	 *  a neighbor of c, according to the above definition.
	 *  The result is at most 8 and at least 0.
	 * 
	 * @param row
	 * @param col
	 * @return the number of living neighbors
	 */
	public int countLivingNeighbors(int row, int col){
		int num=0;
		int minK=row-1;
		int maxK=row+2;
		int minL=col-1;
		int maxL=col+2;
		if (col==cols-1){
			minL=cols-2;
			maxL=cols;
		}
		if (col==0){
			minL=0;
			maxL=2;
		}
		if (row==rows-1){
			minK=rows-2;
			maxK=rows;
		}
		if(row==0){
			minK=0;
			maxK=2;
		}
		for(int k=minK; k<maxK; k=k+1){
			for (int l=minL; l<maxL; l=l+1){
				if (!(k==row && l==col)){
					if (isAlive(k,l)){
						num=num+1;// FIXME
				}
			}
		}
	}
	return num;
	}

	/**
	 * Executes a generation of life.  Be sure to read the specification
	 * for this assignment, because it points out a common mistake students
	 * make when implementing this method.
	 * 
	 */
	public void step(){
		Conway next= new Conway(this.rows, this.cols);
		int alive=0;
		for (int k=0; k<rows; k=k+1){
			for (int l=0; l<cols; l=l+1){
				alive = countLivingNeighbors(k,l);
				if (alive<2 && isAlive(k,l)){
					next.setLiveness(false, k, l);
				}
				if (alive>3 && isAlive(k,l)){
					next.setLiveness(false, k, l);
				}
				if ((alive==2||alive==3)&& isAlive(k,l)){
					next.setLiveness(true, k, l);
				}
				if (alive==3 && !isAlive(k,l)){
					next.setLiveness(true, k, l);
				}
			}
		}
		this.conway=next.conway;// FIXME
	}

	/**
	 * creates a blinker
	 */
	public void blinker() {

		if (this.getRows() < 3 || this.getColumns() < 3) {
			System.out.println("Grid is too small for premade pattern Blinker. " +
					"Conway must be at least 3x3");
		}
		else {
			this.setLiveness(true, 1, 0);
			this.setLiveness(true, 1, 1);
			this.setLiveness(true, 1, 2);
		}
	}

	public void fourBlinkers() {
		clear();
		if (this.getRows() < 9 || this.getColumns() < 9) {
			System.out.println("Grid is too small for premade pattern Four Blinkers. " +
					"Conway must be at least 9x9");
		}
		else {
			for (int i = 3; i < 6; i++) {
				for (int j = 3; j < 6; j++) {
					this.setLiveness(true, i, j);
				}
			}
		}
	}


	public void gosperGliderGun() {
		clear();
		if (this.getRows() < 50 || this.getColumns() < 50) {
			System.out.println("Grid is too small for premade pattern Gosper Glider Gun. " +
					"Conway must be at least 50x50");
		}
		else {
			this.setLiveness(true,0,27);
			this.setLiveness(true,1,25);
			this.setLiveness(true,1,27);
			this.setLiveness(true,2,15);
			this.setLiveness(true,2,16);
			this.setLiveness(true,2,23);
			this.setLiveness(true,2,24);
			this.setLiveness(true,2,37);
			this.setLiveness(true,2,38);
			this.setLiveness(true,3,14);
			this.setLiveness(true,3,18);
			this.setLiveness(true,3,23);
			this.setLiveness(true,3,24);
			this.setLiveness(true,3,37);
			this.setLiveness(true,3,38);
			this.setLiveness(true,4,3);
			this.setLiveness(true,4,4);
			this.setLiveness(true,4,13);
			this.setLiveness(true,4,19);
			this.setLiveness(true,4,23);
			this.setLiveness(true,4,24);
			this.setLiveness(true,5,3);
			this.setLiveness(true,5,4);
			this.setLiveness(true,5,13);
			this.setLiveness(true,5,17);
			this.setLiveness(true,5,19);
			this.setLiveness(true,5,20);
			this.setLiveness(true,5,25);
			this.setLiveness(true,5,27);
			this.setLiveness(true,6,13);
			this.setLiveness(true,6,19);
			this.setLiveness(true,6,27);
			this.setLiveness(true,7,14);
			this.setLiveness(true,7,18);
			this.setLiveness(true,8,15);
			this.setLiveness(true,8,16);


		}
	}

	public void glider() {
		clear();
		if (this.getRows() < 10 || this.getColumns() < 10) {
			System.out.println("Grid is too small for premade pattern Four Blinkers. " +
					"Conway must be at least 10x10");
		}
		else {

			this.setLiveness(true,1,1);
			this.setLiveness(true,1,3);
			this.setLiveness(true,2,2);
			this.setLiveness(true,2,3);
			this.setLiveness(true,3,2);

		}
	}

	public void yourDesignOne() {

	}

	public void yourDesignTwo() {

	}


	public void logAndCapture() {

	}

}
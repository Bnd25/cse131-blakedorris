package lab6;

public class Beer {
	/**	
	 * 
	 * @param n Starting bottles
	 * @return The song with the correct amount of bottles
	 */
	public static String bottles(int n) {
	if (n>0) {
		String song = n +" bottles of beer on the wall, " + n + " bottles of beer. You take one down, pass it around, " + (n-1) + " bottles of beer on the wall. \n" + bottles( n-1)  ;
		return song;
	}
	else {
		return "";
	}
}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       System.out.println(bottles(5));
	}

}

package lab6;

import sedgewick.StdDraw;

public class Triangles {

	/**
	 * 
	 * @param llxlower left hand corner x value
	 * @param llylower left hand y value
	 * @param size The size of each line of the triangle
	 */
	public static void triangle(double llx, double lly, double size) {
	if(size<0.01){
		return;
	}
	
		StdDraw.line(llx, lly, llx+size/2.0, lly+size);
		StdDraw.line(llx, lly, llx+size, lly);
		StdDraw.line(llx+size/2.0, lly+size, llx+size, lly);
		triangle(llx, lly, size/2);
		triangle(llx+size/2.0, lly, size/2);
		triangle(llx+size/4.0, lly+size/2, size/2);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		triangle(0, 0 , 1);
		
	}

}

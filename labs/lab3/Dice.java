package lab3;

import cse131.ArgsProcessor;

public class Dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int diceUsed = ap.nextInt("How many dice will you use?");
		int timesRolled = ap.nextInt("How many times will you roll the dice?");

		int [] diceUsedArray= new int [diceUsed];
		int [] timesRolledArray =new int [timesRolled];
		int [] sums = new int[(diceUsed*6)+1];
		int [] diceTotalArray = new int [timesRolled];
		double allSame =0.0;
		double allSameTotal = 0.0;
		double diceTotal = 0;
		int j = 0;

		for(int i=0; i<timesRolledArray.length; ++i){
			timesRolledArray[i] = i+1;

			int one = 0;
			int two = 0;
			int three = 0;
			int four = 0;
			int five = 0;
			int six = 0;
			allSameTotal = allSameTotal + allSame;

			allSame = 0;


			System.out.print("Round " + timesRolledArray[i] + " \t Dice thrown: ");
			for ( j=0; j<diceUsedArray.length; ++j){

				double chance = Math.random();
				int diceResult = 0;
				if(chance<=(1.0/6.0)&& chance>=0) {
					diceResult = 1;
				}
				if(chance<=(2.0/6.0)&& chance>=(double)(1.0/6.0)) {
					diceResult =2;
				}
				if(chance<=(3.0/6.0)&& chance>=(double)(2.0/6.0))	{
					diceResult = 3;
				}
				if(chance<=(4.0/6.0)&& chance>=(3.0/6.0))    {
					diceResult = 4;
				}
				if(chance<=(5.0/6.0)&& chance>=4.0/6.0)	{
					diceResult = 5;
				}
				if(chance<=1 && chance>= (5.0/6.0)){
					diceResult = 6;
				}
				diceUsedArray [j] = diceResult;
				System.out.print("" + diceResult + " ");
				if (diceResult == 1) {
					diceTotalArray[i] = diceTotalArray[i] + 1;
					one = one +1;
				}
				if (diceResult == 2) {
					diceTotalArray[i] = diceTotalArray[i] + 2;
					two = two +1;
				}
				if (diceResult == 3) {
					diceTotalArray[i] = diceTotalArray[i] + 3;
					three=three+1;
				}
				if (diceResult == 4) {
					diceTotalArray[i] = diceTotalArray[i] + 4;
					four=four+1;
				}
				if (diceResult == 5) {
					diceTotalArray[i] = diceTotalArray[i] +5;
					five=five+1;
				}
				if (diceResult == 6) {
					diceTotalArray[i] = diceTotalArray[i] +6;
					six=six+1;
				}
				if (one == diceUsedArray.length ||two == diceUsedArray.length ||three == diceUsedArray.length ||four == diceUsedArray.length ||five == diceUsedArray.length ||six == diceUsedArray.length){
					allSame = allSame + 1.0;
				}
				if(j==diceUsedArray.length-1) {
					System.out.print("	Sum of the dice: ");

				}



			}

			System.out.print(diceTotalArray[i] + "	\n");
			sums [diceTotalArray[i]]++;
		}

		System.out.println("Percent of rolls having the same number on all dice: " + (double)(Math.round((allSameTotal/(timesRolled))*100000))/1000 + "%.");
		System.out.println("");
		for (int i=(diceUsed*1); i<(diceUsed*6); ++i) {
			if (sums[i] !=0) {
				System.out.println("Sum " + i + " appears " + sums[i] + " times.");
			}

		}























	}

}

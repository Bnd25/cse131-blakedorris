package exercises1;

import cse131.ArgsProcessor;

public class Change {

	public static void main(String[] args) {
		//
		// Below, prompt the user to enter a number of pennies
		//
			ArgsProcessor ap = new ArgsProcessor(args);
			
			int numPennies = ap.nextInt("Starting number of pennies?");
			
			int numDollars = numPennies/100;
			int cents = numPennies % 100;
			int quarters = cents / 25;
				cents = cents % 25;
			int dimes = cents / 10;
				cents = cents % 10;
			int nickels = cents / 5;
				cents = cents % 5;
			int pennies = cents;
			
			
					
					
			
			
			
			
			
			System.out.print ("You have "); System.out.print((numDollars + " Dollars, ")+(quarters + (" quarters, "))+(dimes + (" dimes, "))+(nickels + (" nickels, and ") +(pennies +(" pennies."))));
		
		//
		// Then, compute and print out how many 
		//    dollars, quarters, dimes, nickels, and pennies
		// should be given in exchange for those pennies, so as to
		// minimize the number of coins (see the videos)
		//
	}

}

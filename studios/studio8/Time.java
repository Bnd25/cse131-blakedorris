package studio8;

import java.util.LinkedList;

public class Time {
	private final int hour;
	private final int minute;
	private final boolean twelve;
	
	public Time(int hour, int minute, boolean twelve){
		this.hour=hour;
		this.minute=minute;
		this.twelve = twelve; 
	}
	
	
	
	@Override
	public String toString() {
		if(twelve == true){
			if(hour>12){
		return "Time [hour=" + (hour-12) + ", minute=" + minute + "]PM";
			}
			else {
				return "Time [hour=" + hour + ", minute=" + minute + "]AM";
			}
		}
		else {
			return "Time [hour="  + hour + ", minute=" + minute + "]";
		}
	}

	
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hour;
		result = prime * result + minute;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Time other = (Time) obj;
		if (hour != other.hour)
			return false;
		if (minute != other.minute)
			return false;
		return true;
	}



	public static void main(String[] args) {
		System.out.println(new Time(11, 33, false));
		Time one = new Time(11,33, true);
		Time two = new Time(18, 33, true);
		Time three = new Time(13,00, true);
		LinkedList<Time> otherList= new LinkedList<Time>();
		otherList.add(one);
		otherList.add(two);
		otherList.add(three);
	}

}

package lab6;

public class Methods {

	//
	// In this class, implement the f and g functions described on the lab page
	//
/** 
 * 
 * @param x initial integer
 * @return the final value
 */
	public static int f(int x) {
		if (x>100){
			x=x-10;
			return x;
		}
		else {
			x=f(f(x+11));
			return x;
		}
	}
/**
 * 
 * @param x The first integer input
 * @param y The second integer
 * @return The final value
 */
	public static int g(int x, int y) {
		if(x==0){
			int total = y+1;
			return total;
		}
		if (x>0&& y ==0) {
			int total = g(x-1, 1);
			return total;
		}
		if(x>0&&y>0){
			int total = g(x-1, g(x, y-1));
			return total;
		}
		return 0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//
		// from here, call f or g with the appropriate parameters
		//
		System.out.println(f(99));
		System.out.println(f(87));
		System.out.println(g(1,0));
		System.out.println(g(1,2));
		System.out.println(g(2,2));
		System.out.println(g(4,10));
	}

}

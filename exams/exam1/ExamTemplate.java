package exam1;

import java.lang.reflect.Array;

public class ExamTemplate {
	
	public static int[] question1() {
		// feel free to modify the declaration of answer as necessary
		int[] P = new int[50];
		P[0]= 3;
		P[1]= 0;
		P [2] = 2;
		for (int i = 3; i < P.length; ++i){
			
			P[i]= P[i-2]+P[i-3];
			
			
			
			System.out.println(P[i]);
			//f(x) = f(x-2)+f(x-3);
			
		}
	
		// don't change the return statement
		return P;
	}
	
	public static boolean[] question2() {
		
		// feel free to modify the declaration of answer as necessary
		int[] P = new int[50];
		P[0]= 3;
		P[1]= 0;
		P [2] = 2;
		
		for (int i = 3; i < P.length; ++i){
			
			P[i]= P[i-2]+P[i-3];
		}
		boolean[] primeArray = new boolean[50];
		
		for (int i =3; i<P.length; ++i) {
			boolean prime =  P[i] % i == 0;
			primeArray[i] = prime;
		}

		// don't change the return statement
		return primeArray;
	}
	
	public static int question3() {
		int[] P = new int[50];
		P[0]= 3;
		P[1]= 0;
		P [2] = 2;
		int sum = 0;
		int termCount = 0;
		for (int i = 3; i < P.length; ++i){
			
			P[i]= P[i-2]+P[i-3];
			
		}
		for (int i = 0; sum <= 1000; ++i){
			sum = sum + P[i];
			termCount = termCount +1;
		}
			
		
		
		// don't change the return statement
		return termCount;
	}
	
	public static String[] question4() {
	
		
		//Question 1 array
		int[] P = new int[50];
		P[0]= 3;
		P[1]= 0;
		P [2] = 2;
		for (int i = 3; i < P.length; ++i){
			
			P[i]= P[i-2]+P[i-3];
		}
		
		//Question two array
		
		boolean[] primeArray = new boolean[50];
		
		
		for (int i =3; i<P.length; ++i) {
			boolean prime =  P[i] % i == 0;
			primeArray[i] = prime;
		}
		// feel free to modify the declaration of answer as necessary
		String[] statement = new String[50];
		statement[0]= "0 is NONPRIME";
		statement[1] = "1 is NONPRIME";
		statement[2] = "2 is NONPRIME";
		for (int i = 3; i<P.length; ++i){
			if ( P[i] % i == 0) {
				statement[i] = i + " is prime: " + P[i] + "%" + i + "==0."	;		}
			else {
				statement[i] = i + " is NONPRIME";
			}
		}
		
		
		// don't change the return statement
		return statement;
	}

	public static void main(String[] args) {
		/*
		 * NOTE: 
		 * You should not change and should not need to change any of the code below.
		 * It's there just to make it easy for you to see and verify the accuracy of
		 * your answers.
		 */
		int[] question1Answer = question1();
		boolean[] question2Answer = question2();
		int question3Answer = question3();
		String[] question4Answer = question4();
		
		// print all of the answers
		System.out.println("QUESTION 1 ");
		for (int i = 0; i < question1Answer.length; i++) {
			System.out.print( question1Answer[i] + ", ");
		}
		System.out.println();
		System.out.println();
		
		// print the i's where the value is TRUE
		System.out.println("QUESTION 2 ");
		for (int i = 0; i < question2Answer.length; i++) {
			if (question2Answer[i] == true) {
				System.out.print(i + ", ");
			}
		}
		System.out.println();
		System.out.println();
		
		System.out.println("QUESTION 3: " + question3Answer);
		System.out.println();
		System.out.println();
		
		
		// print out all answers
		System.out.println("QUESTION 4 ");
		for (int i = 0; i < question4Answer.length; i++) {
			System.out.println(question4Answer[i]);
		}

	}

}
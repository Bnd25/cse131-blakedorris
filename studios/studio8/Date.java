package studio8;

import java.util.HashSet;
import java.util.LinkedList;

public class Date {
	private final int month;
	private final int day;
	private final int year;
	private final boolean holiday;
	
	public Date(int month, int day, int year, boolean holiday){
		this.month = month;
		this.day = day;
		this.year = year;
		this.holiday = holiday;
	}


	@Override
	public String toString() {
		return "Date [month=" + month + ", day=" + day + ", year=" + year + ", holiday=" + holiday + "]";
	}

	

	

	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}


	public static void main(String[] args) {
		Date dDay = new Date(6, 6 , 1944, false);
		Date nineEleven = new Date(9,11, 2001, false);
		Date birthday = new Date (9, 23, 1997, false);
		Date otherBirthday = new Date(7, 25, 1997, true);
		Date taBirthday = new Date(10, 27, 1997, false);
		System.out.println(dDay.equals(nineEleven));
		LinkedList<Date> list = new LinkedList<Date>();
		list.add(dDay);
		list.add(nineEleven);
		list.add(birthday);
		list.add(taBirthday);
		list.add(otherBirthday);
		Date d1 = new Date(1,1,2001,true);
		Date d2 = new Date(1,1,2001,true);
		list.add(d1);
		list.add(d2);
		list.add(d1);
		
		System.out.println(list);
		HashSet<Date> set = new HashSet<Date>();
		set.add(d1);
		set.add(d2);
		set.add(d1);
		System.out.println(set);
		
	}
	

}

package onerepmax;

import cse131.ArgsProcessor;

public class OneRepMax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int i = 0;
		int maxWeight = ap.nextInt("How much weight did you lift?");
		int maxWeightReps = ap.nextInt("How many times did you lift that weight?");
		int repsDesired = ap.nextInt("How many reps do you desire to do?");
		int oneRepMax = (int)((double)maxWeight*(1+(double)maxWeightReps/30));
		int repsDesiredWeight = (int)((double)oneRepMax/(1+(double)repsDesired/30));
		System.out.println("One-rep max: " + oneRepMax + "\nWeight for "+repsDesired+ " reps: " + repsDesiredWeight + ".");
		for (i=1; i<=10; ++i){
		System.out.println((100-(5*i))+"% 1 RM: " + (int)oneRepMax*(100-(5*i))/100.0);
	}
		
	}
	

}

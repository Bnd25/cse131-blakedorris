package blakedorrisexam2;


/* 
* Compile Error Deductions: 
* _x_ [-1] Needed to fix a compilation problem in the code
* Brief description of the problem:
* Integer.parseInt to change dragon string to readable format
*/  

public class ExamReturned { 
	public static void main(String[] args) {
		//Yes, this is supposed to be blank

		//Use this method to test the methods that we ask you
		//to write
		System.out.println(dragon(2));

	}

/* Question 1: 10

* __ [+25points] Code produces correct output and follows question specifications appropriately.

* Otherwise, check off as many of these as applicable:

* _x_ [+5] Correct method signature: public static String koch(int n); variable name n can change. Small typos and upper/lower case should not be penalized.
* _x_ [+5] "Correctly identifies the base case - when n==0 returns ""F"""
* __ [+5] "When n > 0 gets koch(n-1)"
* __ [+5] "When n > 0  replaces F's in koch(n-1) with ""F+F-F-F+F"""
*/

	public static String koch(int x) {
		String result = "";
		String kochString = "F+F-F-F+F";

		
		if(x==0){
			return "F";
		}
		if(x>=1){
			for(int i = 1; i<x;++i){
				kochString =  kochString.replaceAll("F", "F+F-F-F+F");
				kochString = kochString;
			}
			return kochString;
		}
		else return "";
		
		//I have no idea how to do this recursively. I know you're supposed to use the method in the answer but I couldn't tell you how
		//I was able to do this one iterively though, since it only had one variable
	}

/* Question 2: 8

* __ [+25points] Code produces correct output and follows question specifications appropriately.

* Otherwise, check off as many of these as applicable:

* _x_ [+4] Correct method signature: public static String dragon(int n); variable name n can change. Small typos and upper/lower case should not be penalized.
* _x_ [+4] "Declares an initial dragonString (name doesn't matter) that equals ""F-H"""
* __ [+4] Uses a for loop i = o; i < n or equivalent while loop 
* __ [+4] Replaces F's with F-H and H's with F+H
* __ [+4] "When replacing F's and H's avoids incorrect additions (e.g. F-H becomes F-H - F+H NOT F-F+H -F+H OR F-H-F-H+H)"
*/

	public static String dragon(int x){
		String result = "";
		String dragonString = "F-H";
		for(int i = 0; i<=x; ++i){

			if(x>0){
				dragonString =  dragonString.replaceAll("F", "F")  +"-" +dragonString.replaceAll("-H", "+H");
				//I thought I could just concatenate these, but it always carries over the
				//letter not replaced by the operation
				//
			}
			else {
				return dragonString;
			}
		}

		return dragonString;

	}

/* Question 3: 0

* __ [+25points] Code produces correct output and follows question specifications appropriately.

* Otherwise, check off as many of these as applicable:

* __ [+4] Correct method signature: public static double computeScale(String lString); variable name lString can change. Small typos and upper/lower case should not be penalized.
* __ [+4] Uses a loop i = 0; i < lString.length() or equivalent while loop
* __ [+4] Uses charAt(i) to get the ith character in the string within the loop
* __ [+4] "When character is either an F or an H adds 1 to a count variable (name substitutions are fine)"
* __ [+4] Returns 1/(2*Math.sqrt(count)) with parentheses that ensure the correct order of operations
*/

	public static double computeScale(int x) {
		return 66.6;
	}
	//The methods that we ask you to write should go here
	//well shit, that was fun
}
// COULDN'T FIND public static void drawLString so grading info for that question is below
/* Question 4: 0

* __ [+25points] Code produces correct output and follows question specifications appropriately.

* Otherwise, check off as many of these as applicable:

* __ [+3] Correct method signature: public static void drawLString(String lString); variable name lString can change. Small typos and upper/lower case should not be penalized.
* __ [+3] Calls computeScale to get the scale value
* __ [+3] Uses a loop i = 0; i < lString.length() or equivalent while loop
* __ [+3] Uses charAt(i) to get the ith character in the string within the loop
* __ [+3] Maintains x and y directions that are used within the StdDraw.line call
* __ [+3] Uses one or more if statement/if-else statements to draw lines for F or H characters
* __ [+3] Uses one or more if statement/if-else statements to change x and y directions for + and - characters
*/


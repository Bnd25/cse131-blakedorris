package project;

import java.awt.Color;


import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class Player implements Drawable{



	private double height;
	private double width;
	private String name;
	private double positionX;
	private double positionY;
	private boolean playerAlive;
	private double ballLocationX;
	private double ballLocationY;
	private double time;
	private int playerWins;
	private double radiusOfBall;
	private PowerUp powerUp;

	public Player(String name, PowerUp powerUp){
		this.height = .025;
		this.width=.025;
		this.name = name;
		this.positionX =0;
		this.positionY = .025;
		this.playerAlive = true;
		this.ballLocationX = 0 ;
		this.ballLocationY = 0;
		this.playerWins = 0;
		this.radiusOfBall = 0.005;
		this.powerUp = powerUp;
	}
	/**
	 * draws the player
	 */
	 public void draw(){

		 StdDraw.text(this.positionX, this.positionY+.025, name);
		 StdDraw.filledRectangle(this.positionX, this.positionY, this.width/2, this.height/2);
	 }
	 public String getName(){
		 return name;
	 }
	 /**
	  * 
	  * @param velocity
	  * @param angle
	  */
	 public void ThrowObject(double velocity, double angle, double radiusOfBall){
		 //
		 //
		 if(powerUp.getUseGravity() == true){
		 double xVelocity = velocity * Math.cos(angle);
		 double yVelocity = velocity * Math.sin(angle);







		 ballLocationX =ballLocationX + xVelocity;
		 ballLocationY =ballLocationY + yVelocity;

		 //	*time -(9.81/10)/2*Math.pow(time, 2)



		 StdDraw.setPenColor(Color.red);
		 getRadius();
		 StdDraw.filledCircle(ballLocationX, ballLocationY, radiusOfBall);
		 time+=1/200000.0;
		 }
		 else{
			 double xVelocity = velocity * Math.cos(angle);
			 double yVelocity = velocity * Math.sin(angle);







			 ballLocationX =ballLocationX + xVelocity;
			 ballLocationY =ballLocationY + yVelocity-(time*9.81);

			 //	*time -(9.81/10)/2*Math.pow(time, 2)



			 StdDraw.setPenColor(Color.red);
			 getRadius();
			 StdDraw.filledCircle(ballLocationX, ballLocationY, radiusOfBall);
			 time+=1/200000.0;
			 
		 }
		 //			StdDraw.show(10);



		 //}

	 }
	 
	 
	 /**
	  * 
	  * @return get radius of the ball
	  */
	 public double getRadius(){
		 if(powerUp.getbigBall()==true){
			 radiusOfBall = .02;

		 }
		 else{
			 radiusOfBall = .005;
		 }
		 return radiusOfBall;
	 }
	 /**
	  * add a win to the total wins for a player
	  */
	 public void addWin(){
		 this.playerWins++;

	 }
	 /**
	  * 
	  * @return the amount of player wins
	  */
	 public int playerWins(){
		 return this.playerWins;
	 }

	 /** 
	  * sets time to zero, time affects gravity
	  */
	 public void setTime(){
		 time = 0;
	 }
	 /**
	  * 
	  * @return x coord for center of player
	  */
	 public double getXCoordinate(){
		 return this.positionX;
	 }
	 /**
	  * 
	  * @return y coord for the center of player
	  */
	 public double getYCoordinate(){
		 return this.positionY;
	 }
	 /**
	  * 
	  * @return x coord of the center of the object(ball)
	  */
	 public double getBallLocationX(){
		 return ballLocationX;
	 }
	 /**
	  * 
	  * @return y coord of the object thrown
	  */
	 public double getBallLocationY(){
		 return ballLocationY;
	 }
	 /**
	  * sets the objects location before changing due to velocity to the position of the player
	  */
	 public void setballLocationToInitial(){
		 this.ballLocationX = this.positionX;
		 this.ballLocationY = this.positionY;
	 }
	 /**
	  * 
	  * @return whether the player is alive or not/ changes this to false
	  */
	 public boolean PlayerHit(){
		 playerAlive= false;
		 return playerAlive;
	 }
	 /**
	  * 
	  * @return whether the player is alive(true) or dead(false)
	  */
	 public boolean getStatus(){
		 return playerAlive;
	 }

	 /**
	  * 
	  * @return whether player has been hit or not
	  */
	 public boolean PlayerAlive(){
		 playerAlive =true;
		 return playerAlive;
	 }
	 /**
	  * 
	  * @return sets x coordinates for player on the left
	  */
	 public double setXCoordinatesLeft(){
		 positionX = 0;
		 while (positionX<.0450||positionX>.46){
			 this.positionX = Math.random();
		 }
		 return this.positionX;
	 }

	 /**
	  * 
	  * @return sets x coordinates for the player on the right
	  */
	 public double setXCoordinatesRight(){
		 positionX = 0;
		 while (positionX<.550||positionX>.960){
			 this.positionX = Math.random();
		 }
		 return this.positionX;
	 }
	 //	public double setYCoordinates(){
	 //		positionY = 0;
	 //		while (positionY>.85||positionY<.15){
	 //			this.positionY = Math.random();
	 //		}
	 //		return this.positionY;
	 //	}
}

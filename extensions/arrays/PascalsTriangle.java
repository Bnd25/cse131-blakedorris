package arrays;
import cse131.ArgsProcessor;

public class PascalsTriangle {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int rows = ap.nextInt("How many rows are in your triangle?");
		int[][]pascals = new int [rows][rows];

		for(int r =0; r<rows; ++r){
			for(int c = 0; c<rows;++c){

				if(c<=0 || r<=0 || c>r){
					pascals[r][c] = 0;
					if( c==0|| c==r){
						pascals[r][c] = 1;
					}
				}

				else {
					pascals[r][c] = pascals[r-1][c]+pascals[r-1][c-1];
				}
				if(pascals[r][c]!=0){
					System.out.print(pascals[r][c]+" ");
				}
			}
			System.out.println("");
		}

	}

}

package studio7;

public class Rectangle {
	
	private double width;
	private double length;
	private double area;
	private double perimeter;
	/**
	 * 
	 * @param x length
	 * @param y width
	 */
	public Rectangle(double length, double width){
		this.length = length;
		this.width = width;
		
	}
	
	public double getArea(){
		return length * width;
	}
	
	public double getPerimeter(){
	return length * 2.0+width * 2.0;
	}
}

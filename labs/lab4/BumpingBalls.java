package lab4;

import java.awt.Color;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {
	
	public static void main(String[] args) {
		
		ArgsProcessor ap = new ArgsProcessor(args);
		StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);
		int i = 0;
		int j = 0;
		int numberOfBalls = ap.nextInt("How many balls do you want?");
		int iterations = ap.nextInt("How many times would you like to calculate the balls positions?");
		double velocityX [] = new double[numberOfBalls];
		double positionX [] = new double[numberOfBalls];
		double velocityY [] = new double[numberOfBalls];
		double positionY [] = new double[numberOfBalls];
		double radius = .05;
		for (i=0; i<numberOfBalls; ++i){
			if (Math.random()<.5){
				positionX[i] = Math.random()-.05;
				positionY[i] = Math.random()-.05;
			}
			else {
				positionX[i] = -Math.random()+.05;
				positionY[i] = -Math.random()+.05;
			}
			velocityX[i] = Math.random()/15.0;
			velocityY[i] = Math.random()/15.0;
			
		}
		
		for (j=0; j<iterations; ++j) {
			StdDraw.clear(Color.gray);
			for (i=0; i<numberOfBalls; ++i) {
				
				StdDraw.setPenColor(Color.red);
				StdDraw.filledCircle(positionX[i], positionY[i], radius);
		
				positionX[i] = positionX[i] + velocityX[i];
				positionY[i] = positionY[i] +velocityY[i];
			
			if (positionX[i] + velocityX[i] > 1.0 - radius)  velocityX[i]=- Math.abs(velocityX[i]);
			if (positionX[i] + velocityX[i] < -1.0 + radius)  velocityX[i]= Math.abs(velocityX[i]);
            if (positionY[i] + velocityY[i] > 1.0 - radius) velocityY[i] =  -Math.abs(velocityY[i]);
            if (positionY[i] + velocityY[i] < -1.0 + radius) velocityY[i] = Math.abs(velocityY[i]);
          
            
            for (int k = numberOfBalls-1; k>i; --k) {
            	if (Math.sqrt((Math.pow(positionX[i]-positionX[k], 2.0))+(Math.pow(positionY[i]-positionY[k], 2.0)))<=radius*2.0+0.1) {
            		velocityX[i]= -velocityX[i];
            		 velocityY[i] = -velocityY[i];
            		 velocityX[k] = -velocityX[k];
            		 velocityY[k] = -velocityY[k];
            	}
            }
			}
			StdDraw.show(15);
		}
		
	}

}

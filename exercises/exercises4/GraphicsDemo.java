package exercises4;

import java.awt.Color;

import sedgewick.StdDraw;

public class GraphicsDemo {

	public static void main(String[] args) {
		StdDraw.setPenColor(Color.blue);
		StdDraw.point(.1, .8);
		
		// blue point (look carefully, will be very small on your screen)
		
		// larger green point
		StdDraw.setPenColor(Color.GREEN);
		StdDraw.filledCircle(.9, .9, .02);
		
		// unfilled red triangle
		StdDraw.setPenColor(Color.red);
		StdDraw.line(.6, 0, 1, 0);
		StdDraw.line(.6, 0, .8, .3);
		StdDraw.line(1, 0, .8, .3);
StdDraw.setPenColor(Color.YELLOW);
		// yellow circle, filled
StdDraw.filledCircle(.1, .1, .3);
		// filled blue rectangle
StdDraw.setPenColor(Color.BLUE);
StdDraw.filledRectangle(.2, .4, .1, .2);


	}

}

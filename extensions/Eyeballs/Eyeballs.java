package Eyeballs;

import java.awt.Color;

import sedgewick.StdDraw;
import cse131.ArgsProcessor;

public class Eyeballs {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		StdDraw.picture(0.5, 0.5, "images/ken.jpg");
		int N = ap.nextInt("How many eyeballs?");
		int clicks = 0;
		double [] mouseX = new double [N];
		double [] mouseY = new double [N];
		double pupil = .01;
		double eyeball = .03;
		
//		for (int j=0; j<N; ++j){
		while(true){
			while (!StdDraw.mousePressed()) {
				
			}
			while (StdDraw.mousePressed()){
				
			}
			
			double ex = StdDraw.mouseX();
			double ey = StdDraw.mouseY();
			if(clicks<=N-1){
				clicks++;
			

			mouseX[clicks-1]=ex;
			mouseY[clicks-1]= ey;
			}
			while ( !StdDraw.mousePressed()){
				StdDraw.clear();
				StdDraw.picture(.5, .5, "images/ken.jpg");
				for(int i=0; i<clicks; ++i){
					
					StdDraw.setPenColor(Color.white);
					
					StdDraw.filledCircle(mouseX[i], mouseY[i], eyeball);
					StdDraw.setPenColor(Color.blue);
					double x = StdDraw.mouseX();
					double p =eyeball - pupil;
					double y = StdDraw.mouseY();
					double dx = mouseX[i]-x;
					double dy = (mouseY[i]- y);
					double angle = Math.atan(dy/dx);

					double px = Math.cos(angle) *p;
					double py = Math.sin(angle) * p;
					if(dx>0&&dy>0){
						px=-px ;
						py=-py;
					}
					if(dx>0&&dy<0){
						px=-px ;
						py=-py;
					}


					StdDraw.filledCircle(mouseX[i]+px, mouseY[i]+py,pupil);
					
				}
				StdDraw.show(10);
				
			}
		}
	}

}

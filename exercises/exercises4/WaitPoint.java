package exercises4;

import sedgewick.StdDraw;

public class WaitPoint {

	public static void main(String[] args) {
		// wait for the mouse to be pressed and released
	
		
		
		while (!StdDraw.mousePressed()) {
			
			
		//do nothing
		StdDraw.pause(100);	
		}

		System.out.println("Pressed");
		// here, the mouse has been pressed

		while (StdDraw.mousePressed()) {
			StdDraw.pause(100);
		
		}
		System.out.println("Released");
		
	
		// here the mouse is released
		
		
		// draw a point at the location of the mouse
		
		
		// here, a q has been typed
		
		
		StdDraw.text(0.5, 0.5, "Farewell!");

	}

}

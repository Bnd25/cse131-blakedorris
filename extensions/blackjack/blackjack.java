package blackjack;

import cse131.ArgsProcessor;

public class blackjack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int players = ap.nextInt("How many players other than you are playing?");
		int games = ap.nextInt("How many games will you play?");
		int [] scores = new int[players+2];
		int [] cards = new int[2];
		int i =0;
		int j = 0;
		int k =0;
		int o =0;
		int cardTotal = 0;
		int playerTotal =0;
		int card = 0;
		int playerWins = 0;
		System.out.println("You have chosen to play "+ games +" games." + "\n" +"There are "+ players + " autonomous players. \n");


		for (k=0; k<games; k++) {
			System.out.println("Game " + (k+1) );
			for (j=0; j<players+2; ++j){

				cardTotal = 0;
				for (i=0; i<2; ++i) {
					 card = (int)(Math.random()*13)+2;
					if (card ==12 || card ==13|| card == 14){
						card = 10;
					}
					cardTotal = cardTotal + card;
					cards[i]= card;
					

				}
				scores[j] = cardTotal;
			}
			System.out.println("The dealer's face-up card has a value of " + cards[0] + ".");
			System.out.println("The player's scores are: ");
			for (j=1; j<=players+1; j++){
				System.out.print(scores[j] + "  ");
				if (j == players+1){
					System.out.println("");

				}
			}
				while (scores[0] <17){
					card = (int)(Math.random()*13)+2;
					if (card ==12 || card ==13|| card == 14){
						card = 10;
					}
					scores[0] = scores[0]+(card);
				}
				for (j=2; j<players+2;++j){
				while (scores[j] <17){
					card = (int)(Math.random()*13)+2;
					if (card ==12 || card ==13|| card == 14){
						card = 10;
					}
					scores[j] = scores[j]+(card);
				}


			}
			System.out.println("The dealer's face-up card has a value of " + cards[0] + " and you have a value of " + scores[1] + ".");
			int hitOrStand = ap.nextInt("Input 1 if you wish to hit or input 2 to stand.");
			while (hitOrStand < 1 || hitOrStand > 2); {
				if (hitOrStand < 1 || hitOrStand > 2){
					hitOrStand = ap.nextInt("Please input a valid number.");
				}
			}
			if (hitOrStand == 1)   {
				playerTotal = scores[1] + (int)(Math.random()*11)+1;
				System.out.println("You chose to hit! \n");
			}
			if (hitOrStand == 2) {
				playerTotal = scores[1];
				System.out.println("You chose to stand! \n");
			}
			
			System.out.println("Dealer got " + scores[0]);
			if (scores[0] <= 21 ) {
				
			
			System.out.println("\n"+ "Human player got " + playerTotal);
			if (playerTotal > scores[0]&& playerTotal<=21){
				System.out.println("Player wins! \n");
				playerWins = playerWins +1;
			}
			if (playerTotal == scores[0]) {
				System.out.println("Player pushes. \n");
			}
			if( playerTotal < scores[0] && playerTotal <=21){
				System.out.println("Player loses. \n");
			}
			if (playerTotal > 21) {
				System.out.println("Player busts! \n");
			}
			for (j=2; j<=players+1; j++) {
				System.out.println("Player " + j + " got " + scores[j]);
				if (scores[j]<scores[0]){
					System.out.println("Player " + j + " loses. \n");
				}
				if (scores[j] == scores[0]){
					System.out.println("Player " + j + " pushes. \n");
				}
				if(scores[j] > scores[0] && scores[j] <=21){
					System.out.println("Player "+ j + " wins. \n");
				}
				if(scores[j] > 21){
					System.out.println("Player "+ j + " busts. \n");
				}
			}
			}
			if(scores[0] > 21) {
				System.out.println("Dealer busts. \n");
				
				
				System.out.println("Human player got " + playerTotal)
				;
				if (playerTotal >0&& playerTotal<=21){
					System.out.println("Player wins! \n");
					playerWins = playerWins +1;
				
				}
				if ( playerTotal >21){
					System.out.println("Player busts.");
				}
				for (j=2; j<=players+1; j++) {
					System.out.println("Player " + j + " got " + scores[j]);
					if (scores[j] >0 && scores[j]<=21){
						System.out.println("Player "+ j + " wins. \n");
					}
					if(scores[j] > 21){
						System.out.println("Player "+ j + " busts. \n");
					}
			}






		}
	
}System.out.print("The percentage of human wins was " + (((double)playerWins/games)*100.0));
System.out.println("%");
}}
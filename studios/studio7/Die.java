package studio7;

public class Die {
	
	private int sides;
	private int nThrows;
	
	public Die(int sides){
		this.sides = sides;
	}
	
	public int getResult(){
		return (int)(Math.random()*sides)+1;
	}
}

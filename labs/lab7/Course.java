package lab7;

public class Course {

	private int seatsLeft;
	private int credits;
	private String Name;
	private Student roster[];
	
	public Course(String Name, int credits, int numberOfSeats){
		this.seatsLeft = numberOfSeats;
		this.Name = Name;
		this.credits = credits;
		this.roster = new Student [numberOfSeats];
		

	}
	public String getName(){
		return this.Name;
	}
	public int getCredits(){
		return this.credits;
	}
	public int getRemainingSeats(){
		return this.seatsLeft;
	}

	public boolean addStudent(Student x){
		if(seatsLeft<=0){return false;

		}
		for(int i = 0; i<(roster.length-seatsLeft);++i){
			if(roster[i].equals(x)){
				return false;
			}

		}
		roster[roster.length-seatsLeft] = x;
		this.seatsLeft = this.seatsLeft-1;
		return true;

	}

	public String generateRoster(){
		String list = "";
		for(int i = 0; i<(roster.length-seatsLeft);++i){
			list = list+ roster[i].toString()+"\n";
		}

		return list;
	}

	public String toString(){
		return Name + credits;
	}
	public double averageGPA(){
		double totalGPA = 0.0;
		
		for(int i = 0; i<(roster.length-seatsLeft);++i){
			double studentGPA = roster[i].getGPA();
			totalGPA = studentGPA + totalGPA;

		}
		return ((double)totalGPA/(roster.length-seatsLeft));

	}
	
	
}


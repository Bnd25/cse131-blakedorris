package studio1;

import cse131.ArgsProcessor;

/**
 * From Sedgewick and Wayne, COS 126 course at Princeton
 * 
 */
public class HiFour {
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String s1 = ap.nextString("What is your name person 1?");
		String s2 = ap.nextString("What is your name person 2?");
		String s3 = ap.nextString("What is your name person 3?");
		String s4 = ap.nextString("What is your name person 4?");
		
		//
		// Say hello to the names in s1 through s4.
		//
		
		System.out.println("Hello " +s1 +", " + s2 + ", " + s3 + ", and " + s4 + ".");

	}
}

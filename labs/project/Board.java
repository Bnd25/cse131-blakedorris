package project;

import java.awt.Color;

import sedgewick.StdDraw;

public class Board implements Drawable{
	private Player p1;
	private Player p2;
	private PowerUp pu1;
	private boolean powerUpHit;

	public Board(Player p1, Player p2, PowerUp pu1){
		this.p1 = p1;
		this.p2 = p2;
		this.pu1 = pu1;
		StdDraw.setCanvasSize(1024, 1000);
		this.powerUpHit = false;

	}
	/**
	 * draws the game board
	 */
	public void draw(){
		StdDraw.clear(Color.cyan);
		StdDraw.setPenColor();

		//	StdDraw.setCanvasSize(980, 980);

		p1.draw();
		p2.draw();
		StdDraw.setPenColor(Color.yellow);
		StdDraw.filledCircle(.2, .8, .06);

		StdDraw.setPenColor(Color.GREEN);

		StdDraw.filledRectangle(.5, 0, .6, .0125);
		StdDraw.setPenColor();
		StdDraw.text(.1, .95, ""+p1.playerWins());
		StdDraw.text(.9, .95, ""+ p2.playerWins());
		if(pu1.getPercentChance()<.35&&getPowerUpHit()==false&&pu1.getSecondChance()==false){
			drawPowerUp();
		}



	}

	/**
	 * 
	 * @param p which player it is; tells who won a round
	 */
	public void winner(Player p ){
		StdDraw.setPenColor();
		StdDraw.text(.5, .5, p.getName() + " wins");
	}

	/**
	 * 
	 * @return whether whether the power up was hit or not
	 */
	public boolean powerUpHit(){
		powerUpHit = true;
		return powerUpHit;

	}
	/**
	 * 
	 * @return that the powerUp has not been hit
	 */
	public boolean powerUpNotHit(){
		powerUpHit = false;
		return powerUpHit;

	}
	/**
	 * 
	 * @return the current status of powerUpHit
	 */
	public boolean getPowerUpHit(){
		return powerUpHit;
	}
	/**
	 * draws the power up circle
	 * 
	 */
	public void drawPowerUp(){


		StdDraw.setPenColor(Color.PINK);


		StdDraw.filledCircle(pu1.getXCoordinates(), pu1.getYCoordinates(), .05);



	}

	//public void drawPlayers(){
	//	StdDraw.filledRectangle(p1.setXCoordinatesLeft(), .03, .03, .05);
	//	StdDraw.filledRectangle(p2.setXCoordinatesRight(), .03, .03, .05);
	//}
}

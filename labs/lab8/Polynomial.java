package lab8;

import java.util.Iterator;
import java.util.LinkedList;

public class Polynomial {

	final private LinkedList<Double> list;

	/**
	 * Constructs a Polynomial with no terms yet.
	 */


	public Polynomial() {
		//
		// Set the instance variable (list) to be a new linked list of Double type
		//
		list = new LinkedList<Double>();   // FIXME
	}

	//	public String toString() {
	//		
	//		return "A polynomial"; // FIXME
	//	}


	public Polynomial addTerm(double coeff) {
		list.add(coeff);
		return this;  // required by lab spec
	}

	@Override
	public String toString() {
		String polynomial = "";
		for(int i = list.size()-1; i>=0;--i){
			double coeff = list.get(i);
			if(i>=2){
				polynomial = polynomial + ""+coeff + "x^" + (i) + " + ";
			}
			if(i==0){
				polynomial = polynomial + ""+coeff ;
			}
			if(i==1){
				polynomial = polynomial + ""+coeff + "x" + " + " ;
			}


		}
		return polynomial;
	}

	public double evaluate(double x) {
		if(list.size()==0){
			return 0;
		}

		else {
			return recursiveEval(x, 0);
		}
	}

	public double recursiveEval(double x, int n){
		if(n==list.size()-1){
			return this.list.get(n);
		}
		else {
			return list.get(n) + x* recursiveEval(x, n+1);

		}

	}

	public Polynomial derivative() {
		Polynomial p = new Polynomial();

		for(int i = 1; i<=list.size()-1;++i){
			if(i==1){
				double coeff = list.get(i);
				p.addTerm(coeff);
			}
			else{
				double coeff = list.get(i)*(i);

				p.addTerm(coeff);
			}


		}

		return p;   // FIXME
	}

	public Polynomial sum(Polynomial another) {
		Polynomial s = new Polynomial ();
		int o = 0;
		int biggestSize = 0;
		boolean original = false;
		if(this.list.size()>=another.list.size()){
			o = another.list.size();
			biggestSize = this.list.size();
			original = true;

		}
		if(this.list.size()<another.list.size()){
			o = list.size();
			biggestSize = another.list.size();
			original = false;
		}
		for(int i=0; i<o; ++i){
			double coeff = list.get(i)+another.list.get(i);
			s.addTerm(coeff);

		}
		if(original == true){
		for(int i = o; i<biggestSize; ++i){
			double coeff = list.get(i);
			s.addTerm(coeff);
		}
		}
		if(original == false){
			for(int i = o; i<biggestSize; ++i){
				double coeff = another.list.get(i);
				s.addTerm(coeff);
		}
		}
		   
		return s;// FIXME
	}
		
	
	/**
	 * This is the "equals" method that is called by
	 *    assertEquals(...) from your JUnit test code.
	 *    It must be prepared to compare this Polynomial
	 *    with any other kind of Object (obj).  Eclipse
	 *    automatically generated the code for this method,
	 *    after I told it to use the contained list as the basis
	 *    of equality testing.  I have annotated the code to show
	 *    what is going on.
	 */

	public boolean equals(Object obj) {
		// If the two objects are exactly the same object,
		//    we are required to return true.  The == operator
		//    tests whether they are exactly the same object.
		if (this == obj)
			return true;
		// "this" object cannot be null (or we would have
		//    experienced a null-pointer exception by now), but
		//    obj can be null.  We are required to say the two
		//    objects are not the same if obj is null.
		if (obj == null)
			return false;

		//  The two objects must be Polynomials (or better) to
		//     allow meaningful comparison.
		if (!(obj instanceof Polynomial))
			return false;

		// View the obj reference now as the Polynomial we know
		//   it to be.  This works even if obj is a BetterPolynomial.
		Polynomial other = (Polynomial) obj;

		//
		// If we get here, we have two Polynomial objects,
		//   this and other,
		//   and neither is null.  Eclipse generated code
		//   to make sure that the Polynomial's list references
		//   are non-null, but we can prove they are not null
		//   because the constructor sets them to some object.
		//   I cleaned up that code to make this read better.


		// A LinkedList object is programmed to compare itself
		//   against any other LinkedList object by checking
		//   that the elements in each list agree.

		return this.list.equals(other.list);
	}

}

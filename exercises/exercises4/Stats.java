package exercises4;

import sedgewick.StdIn;
import cse131.ArgsProcessor;

public class Stats {

	public static void main(String[] args) {
		// prompt the user for the file to be used for this run
		ArgsProcessor.useStdInput("datafiles/average");

		//
		//  Read in the data from the opened file, computing the
		//     sum, average, count, max, and min
		//  of the data
		//
		//  count is the number of doubles read from the file
		//  The other statistics should be clear from their names
		//
		double average = 0;
		double sum = 0;
		int count = 0;
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		while (!StdIn.isEmpty()){
			double d = StdIn.readDouble();
			sum = sum + d;
			count = count +1;
			average = (double)sum/count;
			if (d>max) {
				max= d;
			}
			if (d<min){
				min = d;
			}
			

		}
		System.out.println("The sum is " + sum + ".\nThe average is "+ average + ".\nThe count is "+count +".");
		System.out.println("The max value is: " + max+"\nThe minimum value is "+ min);
	}

}
